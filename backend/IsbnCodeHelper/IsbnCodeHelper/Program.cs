﻿using System;

namespace IsbnCodeHelper
{
    class Program
    {
        static void Main()
        {
            var isbnService = new IsbnService();
            Console.WriteLine(isbnService.IsValid("2049-363001"));
            Console.WriteLine(isbnService.IsValid("2049-363051"));
            Console.WriteLine(isbnService.IsValid("156887115X"));
            Console.WriteLine(isbnService.IsValid(isbnService.Generate()));
            Console.WriteLine(isbnService.IsValid(isbnService.Generate()));
            Console.WriteLine(isbnService.IsValid(isbnService.Generate()));
            Console.WriteLine(isbnService.IsValid(isbnService.Generate()));
            Console.ReadLine();
        }
    }
}
