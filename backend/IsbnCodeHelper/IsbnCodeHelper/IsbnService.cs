﻿using System;
using System.Linq;

namespace IsbnCodeHelper
{
    /// <summary>
    /// Service which can validate ISBN code or generate random ISBN
    /// </summary>
    public class IsbnService
    {
        private const int MaxDigits = 10;
        private const int StartCounter = 10;
        private const int RomanXCharCode = 10;
        private const int CheckNumber = 11;
        private const char RomanXChar = 'X';
        private const char SmallRomanXChar = 'x';

        /// <summary>
        /// Check is ISBN code valid or not
        /// </summary>
        /// <param name="isbn">ISBN code</param>
        /// <returns>Is ISBN valid or not</returns>
        public bool IsValid(string isbn)
        {
            try
            {
                var isbnSum = GetIsbnSum(isbn);
                return isbnSum > 0 && isbnSum % 11 == 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Generate valid random ISBN code
        /// </summary>
        /// <returns>valid random ISBN code</returns>
        public string Generate()
        {
            var result = string.Empty;
            for (var i = 0; i < MaxDigits - 1; i++)
            {
                result += new Random().Next(0, 10).ToString();
            }

            var isbnSum = GetIsbnSum($"{result}0");
            var amountToAdd = CheckNumber - isbnSum % CheckNumber;
            if (amountToAdd == CheckNumber)
            {
                amountToAdd = 0;
            }

            return $"{result}{(amountToAdd == RomanXCharCode ? RomanXChar.ToString() : amountToAdd.ToString())}";
        }

        private static int GetIsbnSum(string isbn)
        {
            if (string.IsNullOrWhiteSpace(isbn))
            {
                throw new ArgumentException($"Incorrect isbn: {isbn}", nameof(isbn));
            }

            var cleanIsbn = new string(isbn.Where(ch => ShouldTakeCharIntoAccount(isbn, ch)).ToArray());
            if (cleanIsbn.Length != MaxDigits)
            {
                throw new ArgumentException($"Incorrect isbn: {isbn}", nameof(isbn));
            }

            var start = StartCounter;
            var result = 0;
            foreach (var x in cleanIsbn)
            {
                result += start * GetInteger(x);
                start--;
            }

            return result;
        }

        private static bool ShouldTakeCharIntoAccount(string isbn, char c) =>
            char.IsDigit(c) ||
            IsRomanXChar(c) && isbn.EndsWith(c) && isbn.Count(x => x == c) == 1;

        private static bool IsRomanXChar(char c) => c == RomanXChar || c == SmallRomanXChar;

        private static int GetInteger(char c)
        {
            if (char.IsDigit(c))
            {
                return int.Parse(c.ToString());
            }
            if (IsRomanXChar(c))
            {
                return RomanXCharCode;
            }

            throw new ArgumentException($"Incorrect character: {c}", nameof(c));
        }
    }
}