﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Mapping.ByCode;
using PostgreSqlProductManager.Migrations;

namespace PostgreSqlProductManager
{
    class Program
    {
        static async Task Main()
        {
            var services = new ServiceCollection();
            services.AddSingleton(typeof(ILogger<>), typeof(ConsoleLogger<>));
            services.AddSingleton(typeof(IStorage<>), typeof(DbStorage<>));
            services.AddSingleton(typeof(IProductService<>), typeof(ProductService<>));


            var connString = "server=localhost;port=5432;database=products_db;user id=postgres;password=111111;";
            new MigrationsRunner(connString).Run();

            var nConf = GetNHConfig(connString);
            using (var factory = nConf.BuildSessionFactory())
            {
                using var session = factory.OpenSession();
                services.AddSingleton(session);
                var serviceProvider = services.BuildServiceProvider();

                var productService = serviceProvider.GetService<IProductService<int>>();
                await productService.AddAsync(new Product<int> { Name = "Name1" });
                await productService.AddAsync(new Product<int> { Name = "Name2" });
                await productService.AddAsync(new Product<int> { Name = "Name3" });
                var product = await productService.GetAsync(1);
                product.Name = "UpdatedName";
                await productService.UpdateAsync(product);
                await productService.DeleteAsync(product);

                product = await productService.GetAsync(1);
                if (product != null)
                {
                    throw new Exception("Error");
                }
                Console.ReadLine();
            }
        }


        private static Configuration GetNHConfig(string conn)
        {
            var config = new Configuration()
                .SetNamingStrategy(ImprovedNamingStrategy.Instance)
                .DataBaseIntegration(db =>
                {
                    db.ConnectionString = conn;
                    db.Dialect<PostgreSQL83Dialect>();
                    db.LogFormattedSql = true;
                    db.LogSqlInConsole = true;
                });
            var mapper = new ModelMapper();
            mapper.AddMappings(Assembly.GetAssembly(typeof(Product<>)).GetExportedTypes());
            var mapping = mapper.CompileMappingForAllExplicitlyAddedEntities();
            config.AddMapping(mapping);
            config.SessionFactory().GenerateStatistics();

            return config;
        }
    }
}
