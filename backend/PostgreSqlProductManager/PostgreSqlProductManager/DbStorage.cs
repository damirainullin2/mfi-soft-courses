﻿using System.Threading.Tasks;
using NHibernate;

namespace PostgreSqlProductManager
{
    public class DbStorage<T> : IStorage<T>
    {
        private readonly ISession _session;

        public DbStorage(ISession session)
        {
            _session = session;
        }

        public async Task<object> SaveAsync(Product<T> product)
        {
            using var tx = _session.BeginTransaction();
            var obj = await _session.SaveAsync(product);
            tx.Commit();
            return obj;
        }

        public async Task UpdateAsync(Product<T> product)
        {
            using var tx = _session.BeginTransaction();
            await _session.UpdateAsync(product);
            tx.Commit();
        }

        public async Task DeleteAsync(Product<T> product)
        {
            using var tx = _session.BeginTransaction();
            await _session.DeleteAsync(product);
            tx.Commit();
        }

        public Task<Product<T>> GetAsync(T id)
        {
            return _session.GetAsync<Product<T>>(id);
        }
    }
}