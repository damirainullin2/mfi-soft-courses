﻿using System.Threading.Tasks;

namespace PostgreSqlProductManager
{
    public interface IStorage<T>
    {
        Task<object> SaveAsync(Product<T> product);

        Task UpdateAsync(Product<T> product);

        Task DeleteAsync(Product<T> product);

        Task<Product<T>> GetAsync(T id);
    }
}