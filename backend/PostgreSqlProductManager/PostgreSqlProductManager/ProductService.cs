﻿using System.Threading.Tasks;

namespace PostgreSqlProductManager
{
    public class ProductService<T> : IProductService<T>
    {
        private readonly IStorage<T> _storage;
        private readonly ILogger<T> _logger;

        public ProductService(IStorage<T> storage, ILogger<T> logger)
        {
            _storage = storage;
            _logger = logger;
        }

        public async Task AddAsync(Product<T> product)
        {
            await _storage.SaveAsync(product);
            _logger.Log("Product added", product);
        }

        public async Task UpdateAsync(Product<T> product)
        {
            await _storage.UpdateAsync(product);
            _logger.Log("Product updated", product);
        }

        public async Task DeleteAsync(Product<T> product)
        {
            await _storage.DeleteAsync(product);
            _logger.Log("Product deleted", product);
        }

        public async Task<Product<T>> GetAsync(T id)
        {
            _logger.Log("Try to get product", id);
            return await _storage.GetAsync(id);
        }
    }
}
