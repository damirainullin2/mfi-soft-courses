﻿namespace PostgreSqlProductManager
{
    public interface ILogger<T>
    {
        void Log(string message, Product<T> product);
        void Log(string message, T id);
    }
}