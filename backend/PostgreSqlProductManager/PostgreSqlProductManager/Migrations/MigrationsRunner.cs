﻿using System;
using FluentMigrator.Runner;
using FluentMigrator.Runner.VersionTableInfo;
using Microsoft.Extensions.DependencyInjection;

namespace PostgreSqlProductManager.Migrations
{
    public class MigrationsRunner
    {
        private readonly string _conn;

        public MigrationsRunner(string conn)
        {
            _conn = conn;
        }

        public void Run()
        {
            var serviceProvider = CreateServices();
            using var scope = serviceProvider.CreateScope();
            var runner = scope.ServiceProvider.GetRequiredService<IMigrationRunner>();
            // Execute the migrations
            runner.MigrateUp();
        }

        private IServiceProvider CreateServices()
        {
            return new ServiceCollection()
                .AddScoped<IVersionTableMetaData, VersionTable>()
                // Add common FluentMigrator services
                .AddFluentMigratorCore()
                .ConfigureRunner(rb => rb
                    // Add support to FluentMigrator
                    .AddPostgres()
                    // Set the connection string
                    .WithGlobalConnectionString(_conn)
                    .WithVersionTable(new VersionTable())
                    // Define the assembly containing the migrations
                    .ScanIn(typeof(BaseMigration).Assembly).For.Migrations())
                // Enable logging to console in the FluentMigrator way
                .AddLogging(lb => lb.AddFluentMigratorConsole())
                // Build the service provider
                .BuildServiceProvider(false);
        }
    }
}
