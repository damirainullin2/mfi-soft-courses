﻿using FluentMigrator;

namespace PostgreSqlProductManager.Migrations
{
    [Migration(1)]
    public class BaseMigration : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Sequence("product_id_seq");
            Create.Table("products")
                .WithColumn("id").AsInt32().NotNullable().PrimaryKey().WithDefaultValue(new NextVal("product_id_seq"))
                .WithColumn("name").AsString().Nullable();
        }
    }
}
