﻿namespace PostgreSqlProductManager.Migrations
{
    public class NextVal
    {
        private readonly string _name;
        public NextVal(string name)
        {
            _name = name;
        }

        public override string ToString()
        {
            return $"nextval('{_name}')";
        }
    }
}