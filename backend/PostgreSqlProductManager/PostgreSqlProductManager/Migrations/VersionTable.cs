﻿using FluentMigrator.Runner.VersionTableInfo;

namespace PostgreSqlProductManager.Migrations
{
    [VersionTableMetaData]
    public class VersionTable : IVersionTableMetaData
    {
        public object ApplicationContext { get; set; }
        public bool OwnsSchema => true;
        public string SchemaName { get; set; }
        public string TableName => "version";
        public string ColumnName => "version_minor";
        public string DescriptionColumnName => "description";
        public string UniqueIndexName { get; }
        public string AppliedOnColumnName => "applied_on";
    }
}