﻿namespace PostgreSqlProductManager
{
    public class Product<T>
    {
        public virtual T Id { get; set; }
        public virtual string Name { get; set; }
    }
}