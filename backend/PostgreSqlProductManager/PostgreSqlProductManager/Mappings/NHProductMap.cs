﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace PostgreSqlProductManager.Mappings
{
    public class NHProductMap : ClassMapping<Product<int>>
    {
        public NHProductMap()
        {
            Table("products");

            Id(x => x.Id, m => m.Generator(Generators.Native));

            Property(x => x.Name, map => map.NotNullable(false));
        }
    }
}