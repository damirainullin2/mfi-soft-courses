﻿using System;
using Newtonsoft.Json.Linq;
using NewtonSoftSerialization.Shapes;

namespace NewtonSoftSerialization
{
    public class ShapeJsonConverter : JsonCreationConverter<Shape>
    {
        protected override Shape Create(Type objectType, JObject jObject)
        {
            var shapeType = Enum.Parse<ShapeType>((string)jObject.Property("type"),
                true);
            return shapeType switch
            {
                ShapeType.Point => (Shape)new Point(),
                ShapeType.Area => new Area(),
                ShapeType.Line => new Line(),
                _ => throw new Exception("IncorrectJSON")
            };
        }
    }
}
