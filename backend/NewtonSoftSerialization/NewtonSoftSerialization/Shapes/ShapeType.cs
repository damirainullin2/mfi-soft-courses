﻿namespace NewtonSoftSerialization.Shapes
{
    public enum ShapeType
    {
        Point,
        Line,
        Area
    }
}