﻿namespace NewtonSoftSerialization.Shapes
{
    public abstract class Shape
    {
        public int Id { get; set; }
        public ShapeType Type { get; set; }
    }
}