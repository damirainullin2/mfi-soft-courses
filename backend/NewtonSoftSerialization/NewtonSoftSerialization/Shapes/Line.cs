﻿namespace NewtonSoftSerialization.Shapes
{
    public class Line : Shape
    {
        public Point From { get; set; }
        public Point To { get; set; }
    }
}