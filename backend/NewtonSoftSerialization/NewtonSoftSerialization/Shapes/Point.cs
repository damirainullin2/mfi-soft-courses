﻿namespace NewtonSoftSerialization.Shapes
{
    public class Point : Shape
    {
        public Coords Coords { get; set; }
    }
}
