﻿namespace NewtonSoftSerialization.Shapes
{
    public class Area : Shape
    {
        public Point[] Points { get; set; }
    }
}