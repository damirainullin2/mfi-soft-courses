﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using NewtonSoftSerialization.Shapes;

namespace NewtonSoftSerialization
{
    class Program
    {
        static void Main()
        {
            using var streamReader = new StreamReader("file.json");
            var serializer = JsonSerializer.Create(new JsonSerializerSettings
            {
                Converters = new List<JsonConverter> { new ShapeJsonConverter() }
            });
            var shapes = serializer.Deserialize<List<Shape>>(new JsonTextReader(streamReader));
            foreach (var shape in shapes)
            {
                Console.WriteLine(shape);
            }
        }
    }
}
