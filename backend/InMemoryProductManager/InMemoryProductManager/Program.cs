﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;

namespace InMemoryProductManager
{
    class Program
    {
        static void Main()
        {
            var services = new ServiceCollection();
            services.AddSingleton(typeof(ILogger<>), typeof(ConsoleLogger<>));
            services.AddSingleton(typeof(IStorage<>), typeof(Storage<>));
            services.AddSingleton(typeof(IProductService<>), typeof(ProductService<>));
            var serviceProvider = services.BuildServiceProvider();
            var productService = serviceProvider.GetService<IProductService<int>>();
            productService.Add(new Product<int> { Id = 1, Name = "Name1" });
            productService.Add(new Product<int> { Id = 2, Name = "Name2" });
            productService.Add(new Product<int> { Id = 3, Name = "Name3" });
            var product = productService.Get(1);
            product.Name = "UpdatedName";
            productService.Update(product);
            productService.Delete(1);
            try
            {
                productService.Get(1);
            }
            catch (KeyNotFoundException)
            {
                Console.ReadLine();
                return;
            }

            throw new Exception("Error");
        }
    }
}
