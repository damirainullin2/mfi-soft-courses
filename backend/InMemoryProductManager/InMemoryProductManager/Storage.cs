﻿using System.Collections.Generic;

namespace InMemoryProductManager
{
    public class Storage<T> : IStorage<T>
    {
        private readonly IDictionary<T, Product<T>> _storageDict =
            new Dictionary<T, Product<T>>();

        public void Save(Product<T> product)
        {
            _storageDict.Add(product.Id, product);
        }

        public void Update(Product<T> product)
        {
            _storageDict[product.Id] = product;
        }

        public void Delete(T id)
        {
            _storageDict.Remove(id);
        }

        public Product<T> Get(T id)
        {
            return _storageDict[id];
        }
    }
}