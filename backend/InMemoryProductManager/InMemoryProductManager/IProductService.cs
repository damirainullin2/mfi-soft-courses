﻿namespace InMemoryProductManager
{
    public interface IProductService<T>
    {
        void Add(Product<T> product);

        void Update(Product<T> product);

        void Delete(T id);

        Product<T> Get(T id);
    }
}