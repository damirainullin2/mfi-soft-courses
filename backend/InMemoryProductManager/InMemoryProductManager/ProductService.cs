﻿namespace InMemoryProductManager
{
    public class ProductService<T> : IProductService<T>
    {
        private readonly IStorage<T> _storage;
        private readonly ILogger<T> _logger;

        public ProductService(IStorage<T> storage, ILogger<T> logger)
        {
            _storage = storage;
            _logger = logger;
        }

        public void Add(Product<T> product)
        {
            _storage.Save(product);
            _logger.Log("Product added", product);
        }

        public void Update(Product<T> product)
        {
            _storage.Update(product);
            _logger.Log("Product updated", product);
        }

        public void Delete(T id)
        {
            _storage.Delete(id);
            _logger.Log("Product deleted", id);
        }

        public Product<T> Get(T id)
        {
            _logger.Log("Try to get product", id);
            return _storage.Get(id);
        }
    }
}
