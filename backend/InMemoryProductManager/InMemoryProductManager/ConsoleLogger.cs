﻿using System;

namespace InMemoryProductManager
{
    public class ConsoleLogger<T> : ILogger<T>
    {
        public void Log(string message, Product<T> product)
        {
            Console.WriteLine(message + ". id = {0}, name = {1}", product.Id.ToString(), product.Name);
        }

        public void Log(string message, T id)
        {
            Console.WriteLine(message + ". id = {0}", id.ToString());
        }
    }
}