﻿namespace InMemoryProductManager
{
    public class Product<TId>
    {
        public TId Id { get; set; }
        public string Name { get; set; }
    }
}