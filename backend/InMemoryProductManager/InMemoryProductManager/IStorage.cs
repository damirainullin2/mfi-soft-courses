﻿namespace InMemoryProductManager
{
    public interface IStorage<T>
    {
        void Save(Product<T> product);
        void Update(Product<T> product);
        void Delete(T id);
        Product<T> Get(T id);
    }
}