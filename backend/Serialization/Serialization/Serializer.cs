﻿using System;
using System.Reflection;
using System.Text;

namespace Serialization
{
    /// <summary>
    /// Custom serializer
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Serializer<T> : IConverter<T> where T : class, new()
    {
        /// <summary>
        /// Serializes <see langword="object"/> to string
        /// </summary>
        /// <param name="obj">object which should be serialized</param>
        /// <returns>serialized string</returns>
        public string Serialize(T obj)
        {
            var type = typeof(T);
            var properties = type.GetProperties();
            var stringBuilder = new StringBuilder();
            foreach (var property in properties)
            {
                var name = GetCaptionName(property);
                stringBuilder.Append($"{name}:{property.GetValue(obj)};");
            }

            var res = stringBuilder.ToString();
            // Remove last char ';'
            return res.Remove(res.Length - 1);
        }

        /// <summary>
        /// Deserialize string to <see langword="object"/> of special type
        /// </summary>
        /// <param name="str">serialized string</param>
        /// <returns>deserialized object</returns>
        public T Deserialize(string str)
        {
            var propertiesStr = str.Split(";");
            var obj = new T();
            var type = typeof(T);
            foreach (var propertyStr in propertiesStr)
            {
                var propertyNameAndValue = propertyStr.Split(":");
                var propertyName = propertyNameAndValue[0];
                var propertyValue = propertyNameAndValue[1];

                var propertyInfo = type.GetProperty(propertyName);
                if (propertyInfo == null)
                {
                    propertyInfo = GetPropertyInfo(type, propertyName);
                }

                var propertyType = propertyInfo.PropertyType;

                //Convert.ChangeType does not handle conversion to nullable types
                //if the property type is nullable, we need to get the underlying type of the property
                var targetType = IsNullableType(propertyType) ? Nullable.GetUnderlyingType(propertyType) : propertyType;

                //Returns an System.Object with the specified System.Type and whose value is
                //equivalent to the specified object.
                var propertyVal = Convert.ChangeType(propertyValue, targetType);

                propertyInfo.SetValue(obj, propertyVal, null);
            }

            return obj;
        }

        private static bool IsNullableType(Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>);
        }

        private static string GetCaptionName(PropertyInfo property)
        {
            var name = property.Name;
            var attrs = property.GetCustomAttributes(true);
            // find caption by propertyName
            foreach (var attr in attrs)
            {
                if (attr is CaptionAttribute captionAttribute)
                {
                    name = captionAttribute.Name;
                }
            }

            return name;
        }

        private static PropertyInfo GetPropertyInfo(Type type, string captionName)
        {
            // find property by caption
            foreach (var property in type.GetProperties())
            {
                var attrs = property.GetCustomAttributes(true);
                foreach (var attr in attrs)
                {
                    if (attr is CaptionAttribute captionAttribute &&
                        captionAttribute.Name == captionName)
                    {
                        return property;
                    }
                }
            }

            throw new ArgumentException($"No captionName {captionName} in type {type}");
        }
    }
}
