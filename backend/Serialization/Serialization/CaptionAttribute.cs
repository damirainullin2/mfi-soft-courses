﻿using System;

namespace Serialization
{
    public class CaptionAttribute : Attribute
    {
        public string Name { get; }

        public CaptionAttribute(string name)
        {
            Name = name;
        }
    }
}