﻿namespace Serialization
{
    public interface IConverter<T>
        where T : class
    {
        string Serialize(T obj);
        T Deserialize(string str);
    }
}