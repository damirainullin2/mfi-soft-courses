﻿using System;

namespace Serialization
{
    class Program
    {
        static void Main()
        {
            var serializer = new Serializer<Model>();
            var model = new Model { Id = 10, Value = "Пример" };
            var modelStr = serializer.Serialize(model);
            Console.WriteLine(modelStr);
            var deserializedModel = serializer.Deserialize(modelStr);
            Console.WriteLine($"Id: {deserializedModel.Id}, Value: {deserializedModel.Value}");
            Console.ReadLine();
        }
    }
}
