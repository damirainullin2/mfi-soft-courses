﻿namespace Serialization
{
    public class Model
    {
        [Caption("Идентификатор")]
        public int Id { get; set; }
        public string Value { get; set; }
    }
}