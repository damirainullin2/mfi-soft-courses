import Cookies from './cookie.js'

window.onload = () => {
    console.log("cookie = " + document.cookie);
    const addCookie = document.getElementById("addCookie");
    addCookie.addEventListener("submit", event => {
        event.preventDefault();
        const cookieName = document.getElementById("cookieName").value;
        const cookieValue = document.getElementById("cookieValue").value;
        const expires = parseInt(document.getElementById("expires").value);
        Cookies.set(cookieName, cookieValue, {expires});
    });
    let i = 0;
    const cookies = Object.entries(Cookies.get());
    for (let [key, value] of Object.entries(cookies)) {
        appendCookie(i++, key, value);
    }

    function appendCookie(index, key, value) {
        const cookieTable = document.getElementById("cookieTable");
        const newRow = cookieTable.insertRow(index);

        let newCell = newRow.insertCell(0);
        let newText = document.createTextNode(key);
        newCell.appendChild(newText);

        newCell = newRow.insertCell(1);
        newText = document.createTextNode(value);
        newCell.appendChild(newText);
    }
};