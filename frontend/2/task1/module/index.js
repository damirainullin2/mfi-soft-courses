class SqrCalc {
    constructor(initValue = 0) {
        SqrCalc._validateNumber(initValue);
        this._result = initValue;
    }

    sum(value) {
        for (let i = 0; i < arguments.length; i++) {
            SqrCalc._validateNumber(arguments[i]);
            this._result += arguments[i];
        }

        return Math.pow(this._result, 2);
    }

    dif(value) {
        for (let i = 0; i < arguments.length; i++) {
            SqrCalc._validateNumber(arguments[i]);
            this._result -= arguments[i];
        }
        return Math.pow(this._result, 2);
    }

    div(value) {
        for (let i = 0; i < arguments.length; i++) {
            SqrCalc._validateNumber(arguments[i]);
            SqrCalc._validateZero(arguments[i]);
            this._result /= arguments[i];
        }
        return Math.pow(this._result, 2);
    }

    mul(value) {
        for (let i = 0; i < arguments.length; i++) {
            SqrCalc._validateNumber(arguments[i]);
            this._result *= arguments[i];
        }
        return Math.pow(this._result, 2);
    }

    static _validateZero(value) {
        if (value === 0) {
            throw  new Error('0 value is inappropriate for this operation.');
        }
    }

    static _validateNumber(value){
        if (isNaN(value)) {
            throw new Error(value + ' is not a number.');
        }
    }
}

module.exports = SqrCalc;