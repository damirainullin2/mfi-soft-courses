function isEqualDeep(var1, var2) {
    const type1 = typeof var1;
    const type2 = typeof var2;
    // if types are different than variables are not equal
    if (type1 !== type2) {
        return false;
    }

    // if we can use "===" to compare
    if (type1 === "string" || type1 === "number" || type1 === "undefined" ||
        var1 === null || var2 === null) {
        return var1 === var2;
    }

    const keys = Object.keys(var1);
    if (keys.length !== Object.keys(var2).length) {
        return false;
    }

    for (let i = 0; i < keys.length; i++) {
        if (var2.hasOwnProperty(keys[i])) {
            if (!isEqualDeep(var1[keys[i]], var2[keys[i]])) {
                return false;
            }
        } else {
            return false;
        }
    }
    return true;
}

module.exports = isEqualDeep;