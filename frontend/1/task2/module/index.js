function mySplice(array, start, deleteCount) {
    if (!array) {
        return array;
    }
    if (arguments.length === 1) {
        return [];
    }
    if (!start) {
        start = 0;
    } else if (start < 0) {
        start = array.length + start;
    }
    if (arguments.length === 2 || start + deleteCount > array.length) {
        // set deleteCount as max in order not to get out of bound of array
        deleteCount = array.length - start;
    }

    // set in res items and delete them from array
    const res = [];
    for (let i = 0; i < deleteCount; i++) {
        res[i] = array[start];
        del(array, start);
    }

    const hasItems = arguments.length > 3;
    if (hasItems) {
        // add items
        for (let i = 0; i < deleteCount; i++) {
            add(array, start + i, arguments[i + 3]);
        }
    }

    return res;
}

// delete element with position from array
function del(arr, pos) {
    for (let i = pos; i < arr.length - 1; i++) {
        arr[i] = arr[i + 1];
    }
    arr.length--;
}

// add element with position to array
function add(arr, pos, el) {
    for (let i = arr.length; i > pos; i--) {
        arr[i] = arr[i - 1];
    }
    arr[pos] = el;
}

module.exports = mySplice;