function consolePrint(array) {
    if (!array)
    {
        return;
    }
    print(array, array.length);
}

function print(array, length) {
    if (length === 0)
    {
        return;
    }
    console.log(array[array.length - length]);
    print(array, length - 1);
}

module.exports = consolePrint;